# Lya WordPress Theme dev


## Unyson integation

### Theme Options

See files in `` ./framework-customizations/theme/options/ `` directory.

### Posts options

See files in `` ./framework-customizations/theme/options/posts/ `` directory.


### Custom shortcodes / page builder elements

See files in `` ./framework-customizations/extensions/shortcodes/shortcodes/ `` directory.

Each shortcode must have an ``options.php`` and a ``views/vew.php`` files.

## Theme-level framework reference

Files in ``./inc/`` directory, matching pattern ``class.class-name.php`` are theme-level framework files.


### Class lya_Option_Patterns

```
./inc/class.option-patterns.php
```

#### get_setting( $setting )

```php

/**
 * Used to avoid copy-pasting of same options over and over again in different files.
 * There are a lot of post loop display shortcodes, having the same options related to querying posts,
 * These repetitive options are all registered there.
 *
 * Usage in options:
 *
 * // example
 * $options = array(
 *   // ....
 *   'population_picker' => lya_Option_Patterns::get_setting( 'population_picker' ),
 *  // ...   
 * );
 * @param string $setting - Settings ID
 */
public static get_setting( $setting: string ) : array|bool
```

#### get_option_default($option: string)

```php

	/**
     *
	 * This method is used to sed default values for options.
	 * It's used when Unyson is NOT INSTALLED, for default theme settings.
	 * @param $option string - Option ID for which we need to retrieve default value.
	 */

public static get_option_default($option: string) : {mixed}

```

### Class lya_PostMeta

```
./inc/class.postmeta.php
```
Used to dynamically get and render all meta-related stuff. This way we're avoiding copy-pasting a lot of meta-related HTML.

### Class lya_Layoyt_Compilator

```
    This class needs refactoring, it was inherited from older themes. It does too many things.
```


